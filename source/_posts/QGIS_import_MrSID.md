---
title: QGIS  import  MrSID (`.sid`) files
categories:
- GIS
tags:
- QGIS
- MrSID
- sid
---

## Use binary conveter provide by lizardtech 
- Register a account at https://www.lizardtech.com/gis-tools/server-development-kit , than download newist sdk 
- extract the file 
- open terminal 
    - ```bash 
cd ./MrSID_DSDK-9.5.4.4709-rhel6.x86-64.gcc482/Raster_DSDK/bin/
```
- run the following  conmmand
    - ```bash
mrsidinfo -i [input file] -of [format,ex "geotiff"] -o [Output file]
```

## Compliling GDAL with MrSID support (Not necessary)
https://gist.github.com/oeon/6527004
